/*
Navicat MySQL Data Transfer

Source Server         : MySql-localhost
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : baizhi

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2020-11-20 17:20:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_product
-- ----------------------------
DROP TABLE IF EXISTS `t_product`;
CREATE TABLE `t_product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(255) NOT NULL COMMENT '商品名',
  `product_price` double(10,2) NOT NULL COMMENT '商品价格',
  `product_dic` varchar(100) NOT NULL COMMENT '商品描述',
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
