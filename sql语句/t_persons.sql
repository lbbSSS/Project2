/*
Navicat MySQL Data Transfer

Source Server         : MySql-localhost
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : baizhi

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2020-11-20 17:20:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_persons
-- ----------------------------
DROP TABLE IF EXISTS `t_persons`;
CREATE TABLE `t_persons` (
  `person_id` int(11) NOT NULL AUTO_INCREMENT,
  `person_name` varchar(20) NOT NULL COMMENT '姓名',
  `person_sex` tinyint(4) NOT NULL COMMENT '性别',
  `person_age` int(11) NOT NULL COMMENT '年龄',
  PRIMARY KEY (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
