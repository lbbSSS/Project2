package com.bz;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @home 10542
 * @auther lbb
 * @date 2020/11/20  17:30
 */
@SpringBootApplication
@MapperScan("com.bz.mapper")
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class,args);
    }

}
