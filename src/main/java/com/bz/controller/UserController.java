package com.bz.controller;


import com.bz.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lbb
 * @since 2020-11-20
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/regist")
    public Map regist(@RequestParam("name") String name, @RequestParam("pwd") String pwd){
        Map map=new HashMap();
        try {
            userService.regist(name,pwd);
            map.put("status","success");
        } catch (Exception e) {
            map.put("status","failed");
        }
        return map;
    }
    @GetMapping("/login")
    public Map login(@RequestParam("name") String name, @RequestParam("pwd") String pwd){
        Map map=new HashMap();
        System.out.println("UserController.login");;
        try {
            userService.login(name,pwd);
            map.put("status","success");
        } catch (Exception e) {
            map.put("status","failed");
        }
        return map;
    }
}

