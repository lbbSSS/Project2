package com.bz.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bz.entity.Product;
import com.bz.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author cyf
 * @since 2020-11-20
 */
@RestController
@RequestMapping("/product")
public class ProductController {
    @Autowired
    private ProductService productService;
    //分页查所有
    @GetMapping("/product")
    public Map selectAll(@RequestParam(defaultValue = "1") Integer page,
                         @RequestParam(defaultValue = "5") Integer limit,
                         @RequestParam(defaultValue = "") String search){
        Page<Product> page1 = new Page<>(page,limit);//设置分页参数
        QueryWrapper<Product> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("Product_name",search);//设置查询条件
        Page<Product> ProductPage1 = productService.page(page1, queryWrapper);
        List<Product> list = ProductPage1.getRecords();//查询到的分页数据
        long total = ProductPage1.getTotal();//查询到的总条数
        System.out.println("total = " + total);
        Map map = new HashMap();
        map.put("count",total);
        map.put("data",list);
        map.put("code",0);
        return map;
        
    }
    //多选删除
    @DeleteMapping("/product")
    public Map deletes(@RequestBody List<Integer> ids){
        Map map = new HashMap();
        try {
            productService.removeByIds(ids);
            map.put("status","success");
        } catch (Exception e) {
            map.put("status","error");
            e.printStackTrace();
        }
        return map;
    }

    //添加
    @PostMapping("/product")
    public Map insert(@RequestBody Product product){
        Map map =new HashMap();
        try {
            productService.save(product);
            map.put("status","success");
        } catch (Exception e) {
            map.put("status","error");
            e.printStackTrace();
        }
        return map;
    }
    //删除
    @DeleteMapping("/product/{id}")
    public Map delete(@PathVariable("id") Integer id){
        Map map = new HashMap();
        try {
            productService.removeById(id);
            map.put("status","success");
        } catch (Exception e) {
            map.put("status","error");
            e.printStackTrace();
        }
        return map;
    }

    //修改
    @PutMapping("/product/{id}")
    public Map update(@PathVariable("id") Integer id,
                      @RequestBody Product product){
        Map map = new HashMap();
        try {
            productService.updateById(product);
            map.put("status","success");
        } catch (Exception e) {
            map.put("status","error");
            e.printStackTrace();
        }
        return map;
    }
}

