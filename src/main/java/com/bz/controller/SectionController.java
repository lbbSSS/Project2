package com.bz.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bz.entity.Section;
import com.bz.service.SectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yml
 * @since 2020-11-20
 */
@RestController
public class SectionController {
    @Autowired
    private SectionService sectionService;

    @GetMapping("/section")
    public Map showPage(Integer page, Integer limit, @RequestParam(defaultValue = "") String search){
        Page<Section> page1 = new Page<>(page, limit);
        QueryWrapper<Section> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("section_name",search);
        Page<Section> page2 = sectionService.page(page1, queryWrapper);
        List<Section> records = page2.getRecords();
        long total = page2.getTotal();
        Map map = new HashMap();
        map.put("count",total);
        map.put("data",records);
        map.put("code",0);
        return map;
    }
    @DeleteMapping("/section")
    public Map deleteAny(@RequestBody List<Integer> ids){
        Map map = new HashMap();
        try {
            sectionService.removeByIds(ids);
            map.put("status","success");
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status","error");
        }
        return map;
    }
    @PostMapping("/section")
    public Map insert(@RequestBody Section section){
        Map map = new HashMap();
        try {
            sectionService.save(section);
            map.put("status","success");
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status","error");
        }
        return map;
    }
    @PutMapping("/section/{id}")
    public Map update(@PathVariable("id") Integer id,@RequestBody Section section){
        Map map = new HashMap();
        try {
            sectionService.updateById(section);
            map.put("status","success");
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status","error");
        }
        return map;
    }
    @DeleteMapping("/section/{id}")
    public Map delete(@PathVariable("id")Integer id){
        Map map = new HashMap();
        try {
            sectionService.removeById(id);
            map.put("status","success");
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status","error");
        }
        return map;
    }
}

