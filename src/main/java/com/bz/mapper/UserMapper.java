package com.bz.mapper;

import com.bz.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lbb
 * @since 2020-11-20
 */
public interface UserMapper extends BaseMapper<User> {

}
