package com.bz.mapper;

import com.bz.entity.Product;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author cyf
 * @since 2020-11-20
 */
public interface ProductMapper extends BaseMapper<Product> {

}
