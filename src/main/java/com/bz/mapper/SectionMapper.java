package com.bz.mapper;

import com.bz.entity.Section;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yml
 * @since 2020-11-20
 */
public interface SectionMapper extends BaseMapper<Section> {

}
