package com.bz.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author cyf
 * @since 2020-11-20
 */
@TableName("t_product")
public class Product implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "product_id", type = IdType.AUTO)
    private Integer productId;

    /**
     * 商品名
     */
    private String productName;

    /**
     * 商品价格
     */
    private Double productPrice;

    /**
     * 商品描述
     */
    private String productDic;


    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(Double productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductDic() {
        return productDic;
    }

    public void setProductDic(String productDic) {
        this.productDic = productDic;
    }

    @Override
    public String toString() {
        return "Product{" +
        "productId=" + productId +
        ", productName=" + productName +
        ", productPrice=" + productPrice +
        ", productDic=" + productDic +
        "}";
    }
}
