package com.bz.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author yml
 * @since 2020-11-20
 */

@TableName("t_section")
public class Section implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "section_id", type = IdType.AUTO)
    private Integer sectionId;

    private String sectionName;

    private String sectionMobile;

    private String sectionAddress;

    public Section() {
    }

    public Section(Integer sectionId, String sectionName, String sectionMobile, String sectionAddress) {
        this.sectionId = sectionId;
        this.sectionName = sectionName;
        this.sectionMobile = sectionMobile;
        this.sectionAddress = sectionAddress;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public void setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public String getSectionMobile() {
        return sectionMobile;
    }

    public void setSectionMobile(String sectionMobile) {
        this.sectionMobile = sectionMobile;
    }

    public String getSectionAddress() {
        return sectionAddress;
    }

    public void setSectionAddress(String sectionAddress) {
        this.sectionAddress = sectionAddress;
    }

    @Override
    public String toString() {
        return "Section{" +
                "sectionId=" + sectionId +
                ", sectionName='" + sectionName + '\'' +
                ", sectionMobile='" + sectionMobile + '\'' +
                ", sectionAddress='" + sectionAddress + '\'' +
                '}';
    }
}
