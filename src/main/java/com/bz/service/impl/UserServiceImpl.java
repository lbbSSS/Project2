package com.bz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bz.entity.User;
import com.bz.mapper.UserMapper;
import com.bz.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lbb
 * @since 2020-11-20
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    private UserMapper userMapper;
    @Override
    public void regist(String name,String pwd) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_name",name);
        User u=userMapper.selectOne(queryWrapper);
        if(u==null){
            u.setUserName(name);
            u.setUserPassword(pwd);
            userMapper.insert(u);
        }else {
            throw  new RuntimeException("用户已被注册");
        }
    }

    @Override
    public void login(String name,String pwd) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_name",name);
        User u=userMapper.selectOne(queryWrapper);
        if(u==null){
            throw  new RuntimeException("用户名不存在");
        }
    }
}
