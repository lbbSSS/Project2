package com.bz.service.impl;

import com.bz.entity.Section;
import com.bz.mapper.SectionMapper;
import com.bz.service.SectionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yml
 * @since 2020-11-20
 */
@Service
public class SectionServiceImpl extends ServiceImpl<SectionMapper, Section> implements SectionService {

}
