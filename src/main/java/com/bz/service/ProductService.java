package com.bz.service;

import com.bz.entity.Product;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cyf
 * @since 2020-11-20
 */
public interface ProductService extends IService<Product> {

}
