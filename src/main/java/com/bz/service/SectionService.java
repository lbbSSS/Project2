package com.bz.service;

import com.bz.entity.Section;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yml
 * @since 2020-11-20
 */
public interface SectionService extends IService<Section> {

}
