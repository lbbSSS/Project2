package com.bz.service;

import com.bz.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lbb
 * @since 2020-11-20
 */
public interface UserService extends IService<User> {

    void regist(String name,String pwd);

    void login(String name,String pwd);
}
