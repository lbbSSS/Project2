package com.bz;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

/**
 * @home 10542
 * @auther lbb
 * @date 2020/11/18  19:31
 */
public class CodeGenerator {
    public static void main(String[] args) {
        //构建代码自动生成器
        AutoGenerator ag = new AutoGenerator();


        //配置连接池参数
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setDriverName("com.mysql.jdbc.Driver");
        dsc.setUrl("jdbc:mysql://localhost:3306/baizhi?useUnicode=true&useSSL=false&characterEncoding=utf-8");
        dsc.setUsername("root");
        dsc.setPassword("root");


        //全局配置
        GlobalConfig gc = new GlobalConfig();
        String propertyPath = System.getProperty("user.dir");//获取当前项目项目路径，截至到项目名,如果是子模块，在定位到子模块
        gc.setOutputDir(propertyPath+"/src/main/java");//代码生成路径
        gc.setAuthor("lbb");//作者名字
        gc.setOpen(false);//是否在代码生成后自动打开目录
        gc.setServiceName("%sService");//去掉service接口前的I


        //配置生成包的规则
        PackageConfig pc = new PackageConfig();
        pc.setParent("com.bz");
        pc.setEntity("entity");
        pc.setMapper("mapper");
        pc.setService("service");
        pc.setServiceImpl("service.impl");
        pc.setController("controller");


        //生成策略
        StrategyConfig sc = new StrategyConfig();
        sc.setInclude("t_user");/*设置通过哪个表生成,如果不指定，默认库中所有表*/
        sc.setNaming(NamingStrategy.underline_to_camel);//数据库中表映射到实体类的命名策略,将数据库中的命名_转换成驼峰
        sc.setColumnNaming(NamingStrategy.underline_to_camel);//数据库表中字段映射到尸体的命名策略，将字段的命名_转换成驼峰
        sc.setTablePrefix("t_");//设置表的前缀，可使在生成表时去掉前缀

        ag.setDataSource(dsc);
        ag.setGlobalConfig(gc);
        ag.setPackageInfo(pc);
        ag.setStrategy(sc);
        ag.execute();
    }
}

/*
//代码生成器依赖
 <dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-generator</artifactId>
    <version>3.3.2</version>
</dependency>
<dependency>
    <groupId>org.apache.velocity</groupId>
    <artifactId>velocity-engine-core</artifactId>
    <version>2.2</version>
</dependency>*/
