package com.bz.config;

import com.baomidou.mybatisplus.extension.plugins.OptimisticLockerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.pagination.optimize.JsqlParserCountOptimize;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @home 10542
 * @auther lbb
 * @date 2020/11/18  15:00
 */
@Configuration
public class MybatisPulsConfig {
    /*开启分页*/
    @Bean
    public PaginationInterceptor paginationInterceptor(){
        PaginationInterceptor paginationInterceptor = new PaginationInterceptor();
        /*开启count的join优化，只针对部分left join */
        paginationInterceptor.setCountSqlParser(new JsqlParserCountOptimize(true));
        /*设置请求的页面大于最大页后操作，true调回到首页，默认false*/
        paginationInterceptor.setOverflow(true);
        /*设置最大单页限制数量，默认500条，最多最多，每页500，，，-1不受限制*/
        paginationInterceptor.setLimit(500);
        return paginationInterceptor;
    }

    /*开启乐观锁，
    * 需要给实体类加上注解
    * @Version
    * private Integer version*/
    @Bean
    public OptimisticLockerInterceptor optimisticLockerInterceptor(){
        return new OptimisticLockerInterceptor();
    }
}
