<%--
  Created by IntelliJ IDEA.
  User: chaiyoufeng
  Date: 2020/11/20
  Time: 20:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <!--引入layui的css和js文件-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="layui/css/layui.css">
    <script type="text/javascript" src="layui/layui.js"></script>
</head>
<body>
<%--界面框--%>
<table id="table" lay-filter="test"></table>
<%--多选删除和添加按钮，搜索表单--%>
<script type="text/html" id="toolbar">
    <%--多选删除--%>
    <button class="layui-btn layui-btn-danger layui-btn-xs" type="button" onclick="handleDeletes()">批量删除<i class="layui-icon layui-icon-delete"/> </button>
    <%--添加按钮--%>
    <button class="layui-btn layui-btn-normal layui-btn-xs" type="button" onclick="handleInsert()">添加<i class="layui-icon layui-icon-addition"/></button>
    <%--搜索表单--%>
    <div class="layui-inline">
        <form action="" class="layui-form">
            <div class="layui-input-inline">
                <input type="text" name="search" id="serach" class="layui-input" placeholder="请输入要查询的商品名">
            </div>
            <div class="layui-input-inline">
                <button onclick="handleSearch()" type="button" class="layui-btn layui-btn-primary"><i class="layui-icon layui-icon-search"></i></button>
            </div>
        </form>
    </div>
</script>
<%--删除和更新按钮--%>
<script type="text/html" id="operaction">
    <button class="layui-btn layui-btn-danger layui-btn-xs" type="button" lay-event="delete">删除</button>
    <button class="layui-btn layui-btn-primary layui-btn-xs" type="button" lay-event="update">更新</button>
</script>
<%--分页查所有--%>
<script type="text/javascript">
    //加载模块
    layui.use(["table","layer","form","laydate"],function () {
        //回调函数
        var table = layui.table;
        var layer = layui.layer;
        var form = layui.form;
        var laydate = layui.laydate;
        var $ = layui.jquery;
        table.render({
            elem:"#table",
            url:"${pageContext.request.contextPath}/product/product",
            cols:[[
                //field:属性名，title：名字，sort：是否排序，fixed：冻结列
                {type:"checkbox"},//显示复选框
                {field:"productId",title:"编号",sort:true},
                {field:"productName",title:"商品名"},
                {field:"productPrice",title:"商品价格"},
                {field:"productDic",title:"商品描述"},
                {title: "操作",toolbar:"#operaction"},//删除和更新
            ]],
            page:true,//开启分页
            limits:[5,10,20],//每页条数的选择项
            limit:5,//每页显示的条数，默认：10
            toolbar:"#toolbar",//删除多选和添加按钮，搜索表单
        })
        //监听lay-filter为test的表格的工具按钮被单机事件，obj是个对象里面有两个属性data表示单击这一行的内容和event的值得内容
        table.on("tool(test)",function (obj) {
            if (obj.event=="delete"){
                //如果是删除操作执行
                $.ajax({
                    url:"${pageContext.request.contextPath}/product/product/"+obj.data.productId,
                    type:"delete",
                    contentType:"application/json",
                    dataType:"json",
                    success:function (result) {
                        if (result.status=="success"){
                            //刷新页面
                            table.reload("table");
                        }else {
                            alert("删除失败");
                        }
                    }
                })
            }else if (obj.event=="update"){
                //如果是修改操作执行
                //弹出层
                layer.open({
                    //弹出层类型
                    type:1,
                    //加载弹出层
                    content:$("#updateForm").html(),
                    success:function (layerObj,index) {
                        //该函数在弹出层成功后立即执行
                        //在该函数中执行对表单和日期组件的手动渲染
                        form.render("radio","updateFrom");
                        //回显数据
                        form.val("updateForm",obj.data);
                        //监听更新表单的提交时间
                        form.on("submit(update-go)",function (data) {
                            //当更新表单提交数据的时候，执行当前函数data.field获取表单最新的数据
                            $.ajax({
                                url:"${pageContext.request.contextPath}/product/product/"+data.field.productId,
                                type:"put",
                                data:JSON.stringify(data.field),
                                contentType:"application/json",
                                dataType:"json",
                                success:function (result) {
                                    if(result.status=="success"){
                                        //关闭弹出层
                                        layer.close(index);
                                        //刷新页面
                                        table.reload("table");
                                    }else {
                                        alert("更新失败");
                                    };
                                }
                            })
                        })
                        $("#update-form-cancel").click(function () {
                            layer.close(index);
                        })
                    },
                })
            }
        })
    })
</script>
<%--删除多选--%>
<script type="text/javascript">
    function handleDeletes() {
        layui.use("table",function () {
            var table = layui.table;
            var $ = layui.jquery;
            var status = table.checkStatus("table");//根据表格的id获取所有选中的行
            var ids = [];
            for (var i=0;i<status.data.length;i++){
                ids[i] = status.data[i].productId;
            }
            if (ids.length==0){
                alert("没有选中的行");
                return;
            }
            $.ajax({
                url:"${pageContext.request.contextPath}/product/product",
                data:JSON.stringify(ids),
                type: "delete",
                contentType:"application/json",
                dataType:"json",
                success:function (result) {
                    if (result.status=="success"){
                        alert("删除成功");
                        //刷新页面
                        table.reload("table");
                    }else {
                        alert("删除失败");
                    }
                }
            })
        })
    }
</script>
<%--添加--%>
<script type="text/javascript">
    function handleInsert() {
        layui.use(["table","form","layer","laydate"],function () {
            var table = layui.table;
            var layer = layui.layer;
            var $ = layui.jquery;
            var form = layui.form;
            //弹出层
            layer.open({
                //弹出层类型
                type:1,
                //加载弹出层
                content:$("#addFrom").html(),
                success:function (layerObj,index) {
                    //该函数在弹出层成功后，立即执行
                    //在该函数中执行对表单和日期组件的手动渲染
                    form.render("radio","addForm");
                    //监听表单的提交事件,data是表单中的数据
                    form.on("submit(go)",function (data) {
                        $.ajax({
                            url:"${pageContext.request.contextPath}/product/product",
                            type:"post",
                            data:JSON.stringify(data.field),
                            contentType:"application/json",
                            dataType: "json",
                            success:function (result) {
                                if(result.status=="success"){
                                    //关闭弹出层
                                    layer.close(index);
                                    //刷新页面
                                    table.reload("table");
                                }else {
                                    alert("添加失败");
                                };
                            }

                        })
                    })
                    $("#add-form-cancel").click(function () {
                        layer.close(index);
                    });
                },
            })
        })
    }
</script>
<%--添加弹出层--%>
<script type="text/html" id="addFrom">
    <form class="layui-form" action="" lay-filter="addForm">
        <div class="layui-form-item">
            <label class="layui-form-label">商品名</label>
            <div class="layui-input-block" style="width: 200px">
                <input type="text" name="productName" required lay-verify="required" placeholder="" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">商品价格</label>
            <div class="layui-input-block" style="width: 200px">
                <input type="text" name="productPrice" required lay-verify="required" placeholder="" autocomplete="off" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">商品描述</label>
            <div class="layui-input-block" style="width: 200px">
                <input type="text" name="productDic" required lay-verify="required" placeholder="" autocomplete="off" class="layui-input" lay-verify="phone">
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block" style="width: 200px">
                <button type="submit" class="layui-btn layui-btn-primary" lay-submit lay-filter="go">添加</button>
                <button type="button" class="layui-btn layui-btn-warm" id="add-form-cancel">取消</button>
            </div>
        </div>
    </form>
</script>
<%--更新弹出层·--%>
<script type="text/html" id="updateForm">
    <form class="layui-form" action="" lay-filter="updateForm">
        <div class="layui-form-item">
            <label class="layui-form-label">商品名</label>
            <div class="layui-input-block" style="width: 200px">
                <input type="hidden" name="productId">
                <input type="text" name="productName" required lay-verify="required" placeholder="" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">商品价格</label>
            <div class="layui-input-block" style="width: 200px">
                <input type="text" name="productPrice" required lay-verify="required" placeholder="" autocomplete="off" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">商品描述</label>
            <div class="layui-input-block" style="width: 200px">
                <input type="text" name="productDic" required lay-verify="required" placeholder="" autocomplete="off" class="layui-input" lay-verify="phone">
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-block" style="width: 200px">
                <button type="submit" class="layui-btn layui-btn-primary" lay-submit lay-filter="update-go">更新</button>
                <button type="button" class="layui-btn layui-btn-warm" id="update-form-cancel">取消</button>
            </div>
        </div>
    </form>
</script>
<%--搜索--%>
<script type="text/javascript">
    function handleSearch() {
        layui.use("table",function () {
            var table = layui.table;
            var $ = layui.jquery;
            table.reload("table",{
                url:"${pageContext.request.contextPath}/product/product",
                where:{
                    //搜索内容
                    "search":$("#serach").val(),
                    //强制每次都从第一页开始
                    "page":1,
                }
            })
        })
    }
</script>
</body>
</html>
