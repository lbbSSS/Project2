package com.baizhi.test;


import com.bz.Application;
import com.bz.entity.Section;
import com.bz.service.SectionService;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @Author: yuanmengling
 * @Date:2020/11/19 10:15
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class TestProjectTest {
    @Autowired
    private SectionService sectionService;
    @Test
    public void testInsert(){
        Section section = new Section(null, "耳鼻喉科", "15484624866", "11楼C区");
        sectionService.save(section);
    }
}
